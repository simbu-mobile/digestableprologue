import 'package:flutter/cupertino.dart';

class NotAuthorised extends StatelessWidget {
  const NotAuthorised({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(child: Text("Not Authorised", style: TextStyle(
    color: CupertinoColors.destructiveRed,
  )));
  }
}