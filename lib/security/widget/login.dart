import 'package:digestable_prologue/security/model/login_result.dart';
import 'package:digestable_prologue/security/provider/authentication_provider.dart';
import 'package:digestable_prologue/security/provider/authentication_service_provider.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'package:go_router/go_router.dart';

class Login extends ConsumerStatefulWidget {
  const Login({super.key});

  @override
  ConsumerState<Login> createState() => _LoginState();
}

class _LoginState extends ConsumerState<Login> {
  late Future<LoginResult> _loginResult;
  @override
  void initState() {
    super.initState();

    _loginResult = ref.read(authenticationServiceProvider).signIn().then((value) => storeAndRedirect(value));
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _loginResult,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            var message =
                snapshot.hasData && snapshot.data!.accessToken.isNotEmpty
                    ? "Bearer:${snapshot.data!.accessToken.substring(0, 25)}..."
                    : snapshot.hasError
                        ? snapshot.error.toString()
                        : snapshot.hasData
                            ? snapshot.data!.message
                            : "No error information";
            return Visibility(
              visible: snapshot.hasData && snapshot.hasError,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    message,
                    style: const TextStyle(color: Colors.white),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: TextButton(
                      style: ButtonStyle(
                          padding: MaterialStateProperty.all<EdgeInsets>(
                              const EdgeInsets.all(15)),
                          foregroundColor:
                              MaterialStateProperty.all<Color>(Colors.red),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side:
                                          const BorderSide(color: Colors.red)))),
                      onPressed: () => ref
                          .read(authenticationServiceProvider)
                          .signIn()
                          .then((value) => (LoginResult value) {
                                storeAndRedirect(value);
                              }(value)),
                      child: const Text(
                        "Login",
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                  ),
                ],
              ),
            );
          } else {
            return Container();
          }
        });
  }

  storeToken(WidgetRef ref, LoginResult value) {
    ref
        .read(authenticationProvider.notifier)
        .storeAccessToken(value.isLoggedIn ? value.accessToken : "");
  }

  storeAndRedirect(LoginResult value) {
    storeToken(ref, value);
    if (value.isLoggedIn) {
      GoRouter.of(context).push("/");
    }
  }
}
