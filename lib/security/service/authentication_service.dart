import 'package:digestable_prologue/security/model/login_result.dart';
import 'package:digestable_prologue/security/model/logout_result.dart';
import 'package:simbucore/app/event_store/service/event_store.dart';

import 'package:digestable_prologue/security/service/msal_authentication_service.dart'
    if (dart.library.js) 'package:digestable_prologue/security/service/msal_js_authentication_service.dart';

class AuthenticationService {
  static const authenticationServiceTypeNameStubbed = "stubbed";
  static const authenticationServiceTypeNameLive = "live";
  static const authenticationServiceTypeNameUat = "uat";

  static AuthenticationService get instance {
    return getAuthenticationService();
  }

  String typeName = "";

  Future<LoginResult> signIn() async {
    return LoginResult("id", "name", "accessToken", "message");
  }

  Future<LogoutResult> signOut(EventStore eventStore) async {
    return LogoutResult(true, "");
  }
}
