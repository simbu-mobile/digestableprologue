import 'package:digestable_prologue/security/model/logout_result.dart';
import 'package:digestable_prologue/security/model/login_result.dart';
import 'package:digestable_prologue/security/service/authentication_service.dart';
import 'package:simbucore/app/core/service/global_environment_values.dart';
import 'package:simbucore/app/event_store/service/event_store.dart';
import 'package:msal_js/msal_js.dart';

AuthenticationService getAuthenticationService() =>
    MsalJsAuthenticationService();

class MsalJsAuthenticationService implements AuthenticationService {
  PublicClientApplication? publicClientApp;
  String initError = "";

  @override
  String typeName = AuthenticationService.authenticationServiceTypeNameLive;

  @override
  Future<LoginResult> signIn() async {
    return await _getResult(false);
  }

  @override
  Future<LogoutResult> signOut(EventStore eventStore) async {
    await _intPca();
    String res = "No error message";
    try {
      publicClientApp!.logoutRedirect();
      return LogoutResult(true, "");
    } on AuthException catch (ex) {
      res = ex.message;
    }
    return LogoutResult(false, res);
  }

  Future _intPca() async {
    if (publicClientApp != null) {
      return;
    }

    try {
      publicClientApp = PublicClientApplication(
        Configuration()
          ..auth = (BrowserAuthOptions()
            ..clientId = GlobalEnvironmentValues.instance.applicationClientId
            ..authority = GlobalEnvironmentValues.instance.authority)
          ..system = (BrowserSystemOptions()
            ..loggerOptions = (LoggerOptions()
              ..loggerCallback = _loggerCallback
              ..logLevel = LogLevel.error)),
      );
    } on Exception catch (ex) {
      initError = ex.toString();
    }
  }

  Future<LoginResult> _getResult(bool isSilent) async {
    AuthenticationResult? result;
    AccountInfo;
    String? res;

    await _intPca();

    try {
      result = await publicClientApp!
          .acquireTokenSilent(
              SilentRequest()..scopes = GlobalEnvironmentValues.instance.scopes);

      if (result.account?.homeAccountId == null) {
        await publicClientApp!.acquireTokenPopup(
            PopupRequest()..scopes = GlobalEnvironmentValues.instance.scopes);
      }

      var loginResult = LoginResult(
          result.account?.homeAccountId ?? "",
          result.account?.username ?? "",
          result.accessToken,
          result.account == null ? "failed no msg" : "success");
      return loginResult;
    } on AuthException catch (ex) {
      res = ex.message;
    }

    return LoginResult("", "", "", "Failed: $res");
  }

  // Pii indicates that the message contains Personally Identifiable Information from the request to the MS Identity server.
  void _loggerCallback(LogLevel level, String message, bool containsPii) {
    var writer = GlobalEnvironmentValues.instance.logWriter;
    switch (level) {
      case LogLevel.error:
        {
          writer.error(message);
        }
        break;
      case LogLevel.warning:
        {
          writer.warn(message);
        }
        break;
      case LogLevel.info:
        {
          writer.info(message);
        }
        break;
      default:
        {
          writer.debug(message);
        }
        break;
    }
  }
}
