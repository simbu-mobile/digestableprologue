import 'package:digestable_prologue/security/event/signed_out.dart';
import 'package:digestable_prologue/security/model/login_result.dart';
import 'package:digestable_prologue/security/model/logout_result.dart';
import 'package:digestable_prologue/security/service/authentication_service.dart';
import 'package:simbucore/app/event_store/service/event_store.dart';

class StubbedAuthenticationService implements AuthenticationService {
  @override
  String typeName = AuthenticationService.authenticationServiceTypeNameStubbed;
  
  @override
  Future<LoginResult> signIn() async { 
    // Fake successful authentication, used for automated test and development.
    return LoginResult("1", "joe", "token", "success");
  }
  
  @override
  Future<LogoutResult> signOut(EventStore eventStore) async {
    eventStore.bus.fire(SignedOut());
    return LogoutResult(true, "");
  }
}
