import 'package:azure_ad_authentication/azure_ad_authentication.dart';
import 'package:azure_ad_authentication/exeption.dart';
import 'package:azure_ad_authentication/model/user_ad.dart';
import 'package:digestable_prologue/security/model/login_result.dart';
import 'package:digestable_prologue/security/model/logout_result.dart';
import 'package:digestable_prologue/security/service/authentication_service.dart';
import 'package:flutter/services.dart';
import 'package:simbucore/app/core/service/global_environment_values.dart';
import 'package:simbucore/app/event_store/service/event_store.dart';

AuthenticationService getAuthenticationService() => MsalAuthenticationService();

class MsalAuthenticationService implements AuthenticationService {
  static String authFailedMsg = "authentication failed";
  static String silentTokenAcquireFailedMsg = "silent token acquire failed";

  @override
  String typeName = AuthenticationService.authenticationServiceTypeNameLive;

  @override
  Future<LoginResult> signIn() async {
    return await _getResult();
  }

  @override
  Future<LogoutResult> signOut(EventStore eventStore) async {
    AzureAdAuthentication pca = await _intPca();
    String res;
    try {
      await pca.logout();
      return LogoutResult(true, "");
    } on MsalException {
      res = "Error signing out";
    } on PlatformException catch (e) {
      res = "some other exception ${e.toString()}";
    }
    return LogoutResult(false, res);
  }

  Future<AzureAdAuthentication> _intPca() async {
    return await AzureAdAuthentication.createPublicClientApplication(
        clientId: GlobalEnvironmentValues.instance.applicationClientId,
        authority: GlobalEnvironmentValues.instance.authority);
  }

  LoginResult _mapADModelToUserResult(UserAdModel? userAdModel) {
    return LoginResult(
        userAdModel?.id ?? "",
        userAdModel?.userPrincipalName ?? "",
        userAdModel?.accessToken ?? "",
        userAdModel == null ? authFailedMsg : "success");
  }

  Future<LoginResult> _getResult() async {
    AzureAdAuthentication pca = await _intPca();
    String? res;
    UserAdModel? userAdModel;
    try {
      var authScopes = GlobalEnvironmentValues.instance.scopes;
      userAdModel =
          await pca.acquireTokenSilent(scopes: authScopes).catchError((e) {
        return null;
      });
      userAdModel ??= await pca.acquireToken(scopes: authScopes);
      return _mapADModelToUserResult(userAdModel);
    } on MsalUserCancelledException {
      res = "User Cancelled";
    } on MsalNoAccountException {
      res = "No Account";
    } on MsalInvalidConfigurationException {
      res = "Invalid Config";
    } on MsalInvalidScopeException {
      res = "Invalid Scope";
    } on MsalException {
      res = "Error getting token. Unspecified reason";
    }

    return LoginResult("", "", "", "Failed: $res");
  }
}
