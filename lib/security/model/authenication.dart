import 'package:digestable_prologue/security/model/authentication_mode.dart';

/// Shows if a users is authenticated and if authentication is required
class Authentication{
  final AuthenticationMode authenticationMode;
  final String userName;
  final String accessToken;

  bool get isAuthenticated => accessToken.replaceAll(' ', '').isNotEmpty;

  Authentication(this.authenticationMode, this.userName, this.accessToken);
}