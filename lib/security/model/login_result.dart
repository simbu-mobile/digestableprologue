class LoginResult {
  final String id;
  final String name;
  final String accessToken;
  final String message;

  bool get isLoggedIn => message == "success";

  LoginResult(this.id, this.name, this.accessToken, this.message);
}
