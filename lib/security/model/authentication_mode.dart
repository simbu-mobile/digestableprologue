/// Use to control when Authentication is required and applied.
enum AuthenticationMode {
  alwaysOn, // Requires the user to be authenticated have access and will request login when not authenticated
  apiOnly, // Only attempt to authenticate at the point we make an API request
}
