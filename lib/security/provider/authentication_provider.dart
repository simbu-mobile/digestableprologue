// Package imports:
import 'package:digestable_prologue/security/model/Authenication.dart';
import 'package:digestable_prologue/security/model/authentication_mode.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class AuthenticationStateNotifier
    extends StateNotifier<Authentication> {
  AuthenticationStateNotifier(super.state);

  setAuthenicationMode(AuthenticationMode authenticationMode) {
    state = Authentication(authenticationMode, state.userName, state.accessToken);
  }

  storeAccessToken(String accessToken) {
    state = Authentication(state.authenticationMode, state.userName, accessToken);
  }
}

final authenticationProvider = StateNotifierProvider<
    AuthenticationStateNotifier, Authentication>(
  (ref) => AuthenticationStateNotifier(Authentication(AuthenticationMode.alwaysOn, "", "")),
);
