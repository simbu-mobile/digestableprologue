// Package imports:
import 'package:digestable_prologue/security/service/authentication_service.dart';
import 'package:digestable_prologue/security/service/stubbed_authentication_service.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:simbucore/app/core/model/environments.dart';
import 'package:simbucore/app/core/provider/platform_provider.dart';
import 'package:simbucore/app/core/service/global_environment_values.dart';
import 'package:universal_platform/universal_platform.dart';

class AuthenticationServiceStateNotifier
    extends StateNotifier<AuthenticationService> {
  AuthenticationServiceStateNotifier(super.state);
}

AuthenticationServiceStateNotifier selectAuthenticationServiceByEnvironment(
    UniversalPlatformType platform) {
  var environment = GlobalEnvironmentValues.instance.environment;

  AuthenticationService authenticationService = environment == Environments.live
      ? AuthenticationService.instance
      : StubbedAuthenticationService();

  return AuthenticationServiceStateNotifier(authenticationService);
}




final authenticationServiceProvider = StateNotifierProvider<
    AuthenticationServiceStateNotifier, AuthenticationService>(
  (ref) => 
      selectAuthenticationServiceByEnvironment(ref.watch(platformProvider)),
);


