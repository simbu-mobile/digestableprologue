import 'package:digestable_prologue/security/service/secure_route_service.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final secureRouteProvider = StateProvider<SecureRouteService>(
  // We return the default sort type, here name.
  (ref) => SecureRouteService(),
);