// Package imports:
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:digestable_prologue/connection_detector/model/network_connection_state.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class NetworkConnectionStateNotifier
    extends StateNotifier<NetworkConnectionState> {
  NetworkConnectionStateNotifier(super.state);

  setNetworkStateByConnectivityResult(ConnectivityResult connectivityResult) {
    var onlineStates = <ConnectivityResult>[
      ConnectivityResult.wifi,
      ConnectivityResult.ethernet,
      ConnectivityResult.mobile
    ];

    var isOnlineState = onlineStates.contains(connectivityResult);

    state = isOnlineState
        ? NetworkConnectionState.online
        : NetworkConnectionState.offline;
  }
}

final networkConnectionStateProvider = StateNotifierProvider<
    NetworkConnectionStateNotifier, NetworkConnectionState>(
  (ref) => NetworkConnectionStateNotifier(NetworkConnectionState.offline),
);
