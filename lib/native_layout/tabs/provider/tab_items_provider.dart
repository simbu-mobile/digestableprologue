// Package imports:
import 'package:layout_lego/models/dl_tab_item.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class TabItemsNotifier extends StateNotifier<List<DlTabItem>> {
  TabItemsNotifier(super.state);

  set(List<DlTabItem> tabItems){
    state = tabItems;
  }
}

final tabItemsProvider = StateNotifierProvider<TabItemsNotifier, List<DlTabItem>>(
  (ref) => TabItemsNotifier([]),
);
