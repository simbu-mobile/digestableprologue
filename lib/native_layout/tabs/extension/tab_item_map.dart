// Package imports:
import 'package:layout_lego/models/dl_tab_item.dart';

extension TabItemMap on List<DlTabItem> {
  List<DlTabItem> toDlTabItems() {
    return map((item) => DlTabItem(
          label: item.label,
          icon: item.icon,
          content: item.content,
          globalKey: item.globalKey,
          navigationBar: null
        )).toList();
  }
}
