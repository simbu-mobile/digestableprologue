// Flutter imports:
import 'package:digestable_prologue/native_layout/tabs/provider/tab_items_provider.dart';
import 'package:flutter/cupertino.dart';

// Package imports:
import 'package:layout_lego/dl_bottom_tab_layout.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:simbucore/theme/provider/theme_provider.dart';
import 'package:universal_platform/universal_platform.dart';

// Project imports:
import 'package:simbucore/app/core/extension/platform_type_map.dart';
import 'package:simbucore/app/core/model/device.dart';
import 'package:simbucore/app/core/provider/device_provider.dart';
import 'package:simbucore/app/core/provider/platform_provider.dart';
import 'package:digestable_prologue/native_layout/sidedraw/widget/android_sidedraw_layout.dart';
import 'package:digestable_prologue/native_layout/tabs/extension/tab_item_map.dart';

/// Select the best layout for the client device, to give a native look and feel.
class LayoutSelector extends ConsumerWidget {
  const LayoutSelector({Key? key}) : super(key: key);

  Widget selectLayout(BuildContext context, WidgetRef ref, Device device) {
    if (device.isIPhone) {
      return DlBottomTabLayout(
        ref.read(platformProvider).toDlPlatformType(),
        ref.watch(tabItemsProvider).toDlTabItems(),
      );
    }

    if (ref.watch(platformProvider) == UniversalPlatformType.Android) {
      return const SidedrawLayout();
    }

    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          "Your platform or device '${device.name}', is not supported by this application.",
          style: TextStyle(color: ref.watch(themeProvider).trafficLightRed),
        ),
      ),
    );
  }

  /// Run the best application for the platform of the device using the application, to provide a better native experience.
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ref.watch(deviceProvider).when(
          data: (data) => selectLayout(context, ref, data),
          loading: () => Container(),
          error: (error, stack) => Container(),
        );
  }
}
