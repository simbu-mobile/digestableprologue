// Package imports:
import 'package:hooks_riverpod/hooks_riverpod.dart';

// Project imports:
import 'package:digestable_prologue/native_layout/sidedraw/model/Sidedraw.dart';

class SidedrawNotifier extends StateNotifier<Sidedraw> {
  SidedrawNotifier(super.state);

  initialise(int index, String headerLabel){
    state = Sidedraw(index, headerLabel);
  }
  
  setIndex(int index) {
    state = Sidedraw(index, state.headerLabel);
  }
}

final sidedrawProvider = StateNotifierProvider<SidedrawNotifier, Sidedraw>(
  (ref) => SidedrawNotifier(Sidedraw(0, "")),
);
