// Flutter imports:
import 'package:digestable_prologue/native_layout/tabs/provider/tab_items_provider.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:layout_lego/models/dl_tab_item.dart';
import 'package:layout_lego/models/dl_navigation_bar.dart';

import 'package:hooks_riverpod/hooks_riverpod.dart';

// Project imports:
import 'package:digestable_prologue/native_layout/sidedraw/provider/sidedraw_provider.dart';
import 'package:digestable_prologue/native_layout/sidedraw/widget/android_sidedraw.dart';

class SidedrawLayout extends ConsumerWidget {
  const SidedrawLayout({Key? key}) : super(key: key);

  AppBar appBar(DlNavigationBar navigationBar) {
    return AppBar(title: Text(navigationBar.title));
  }

  Scaffold tab(BuildContext context, DlTabItem tabItem) {
    return Scaffold(
      appBar: appBar(tabItem.navigationBar ?? const DlNavigationBar(title: "")),
      body: tabItem.content,
      drawer: const AndroidSideDraw(),
    );
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return IndexedStack(
      index: ref.watch(sidedrawProvider).index,
      children: [
        for (var tabItem in ref.watch(tabItemsProvider)) tab(context, tabItem),
      ],
    );
  }
}
