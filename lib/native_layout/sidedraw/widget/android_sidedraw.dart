// Flutter imports:
import 'package:digestable_prologue/native_layout/tabs/provider/tab_items_provider.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:layout_lego/models/dl_tab_item.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:simbucore/theme/provider/theme_provider.dart';

// Project imports:
import 'package:digestable_prologue/native_layout/sidedraw/provider/sidedraw_provider.dart';

// https://codesinsider.com/flutter-navigation-drawer-example-tutorial/

class AndroidSideDraw extends ConsumerWidget {
  const AndroidSideDraw({Key? key}) : super(key: key);

  ListTile listTile(
      BuildContext context, DlTabItem tabItem, int index, WidgetRef ref) {
    return ListTile(
      title: Text(tabItem.label ?? ""),
      leading: IconButton(
        icon: tabItem.icon,
        onPressed: () {},
      ),
      onTap: () {
        // Update the state of the app
        ref.read(sidedrawProvider.notifier).setIndex(index);
        // Then close the drawer;
        Navigator.pop(context);
      },
    );
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var themer = ref.watch(themeProvider);

    return Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            decoration: const BoxDecoration(
              color: Colors.blue,
            ),
            child: Text(ref.watch(sidedrawProvider).headerLabel,
                style: themer.typo.title3),
          ),
          //for (var tabItem in referenceData.tabItems) listTile(context, tabItem),
          for (int i = 0; i < ref.watch(tabItemsProvider).length; i++)
            listTile(context, ref.watch(tabItemsProvider)[i], i, ref),
        ],
      ),
    );
  }
}
