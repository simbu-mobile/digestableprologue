/* 
  Android side draw layout view model
*/

class Sidedraw {
  final int index;
  final String headerLabel;

  Sidedraw(this.index, this.headerLabel);
}