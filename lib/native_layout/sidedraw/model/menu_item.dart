// Flutter imports:
import 'package:flutter/widgets.dart';

class MenuItemVM {
  final Icon icon;
  final String label;
  MenuItemVM(this.icon, this.label);
}
