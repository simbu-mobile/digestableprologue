import 'package:digestable_prologue/security/event/signed_out.dart';
import 'package:flutter/cupertino.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:simbucore/app/event_store/provider/event_provider.dart';

class Settings extends ConsumerStatefulWidget {
  const Settings({super.key});

  @override
  ConsumerState<Settings> createState() => _SettingsState();
}

class _SettingsState extends ConsumerState<Settings> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color(0xFFEFEFF4),
      width: double.infinity,
      height: double.infinity,
      child: SafeArea(
        child: SettingsList(
          sections: [signoutSection(context)],
        ),
      ),
    );
  }

  AbstractSettingsSection signoutSection(BuildContext context) {
    var section = SettingsSection(
      tiles: <SettingsTile>[
        SettingsTile(
          title: const Center(
              child: Text(
            "Sign Out",
            style: TextStyle(color: CupertinoColors.systemRed),
          )),
          onPressed: (context) => {ref.read(eventStoreProvider).bus.fire(SignedOut())},
        ),
      ],
    );
    return section;
  }
}
