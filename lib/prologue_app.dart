// Flutter imports:
import 'dart:async';

import 'package:digestable_prologue/security/widget/login.dart';
import 'package:digestable_prologue/app/settings/widget/settings.dart';
import 'package:simbucore/app/core/extension/platform_type_map.dart';
import 'package:simbucore/app/core/provider/app_lifecycle_state_provider.dart';
import 'package:digestable_prologue/connection_detector/provider/network_connection_state_provider.dart';
import 'package:digestable_prologue/native_layout/widget/layout_selector.dart';
import 'package:digestable_prologue/splash_screen/event/preserve_splash_screen.dart';
import 'package:digestable_prologue/splash_screen/event/remove_splash_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:simbucore/app/core/service/global_environment_values.dart';
import 'package:simbucore/app/core/service/provider_resolver.dart';
import 'package:simbucore/app/event_store/provider/event_provider.dart';
import 'package:simbucore/app/core/model/application_mode.dart';
import 'package:simbucore/app/core/provider/application_mode_provider.dart';
import 'package:simbucore/app/core/provider/platform_provider.dart';
import 'package:simbucore/app/logging/provider/log_writer_provider.dart';
import 'package:simbucore/app/routes/model/a_route.dart';
import 'package:simbucore/app/routes/provider/route_registry_provider.dart';
import 'package:simbucore/app/routes/service/navigation_events_observer.dart';
import 'package:simbucore/app/routes/service/routes_to_gorouter_adapter.dart';
import 'package:simbucore/theme/provider/theme_provider.dart';
import 'package:universal_platform/universal_platform.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

class PrologueApp extends ConsumerStatefulWidget {
  const PrologueApp({Key? key}) : super(key: key);

  @override
  ConsumerState<PrologueApp> createState() => _PrologueAppState();
}

class _PrologueAppState extends ConsumerState<PrologueApp>
    with WidgetsBindingObserver {
  late StreamSubscription<ConnectivityResult> subscription;

  @override
  void initState() {
    super.initState();

    // Binds to application events to allow overrides e.g. didChangePlatformBrightness
    WidgetsBinding.instance.addObserver(this);

    // Allow a number of key services to get access to the providers outside the build context.
    ProviderResolver.setRef = ref;

    // Keep showing the splash screen until the application finisheds loading.
    ref.read(eventStoreProvider).bus.fire(PreserveSplashScreen());

    // Register routes
    var routeRegistry = ref.read(routeRegistryProvider);
    routeRegistry
        .add(ARoute("Home", "Landing Page", "/", const LayoutSelector()));
    routeRegistry.add(ARoute("Login", "Login Screen", "/login", const Login()));
    routeRegistry.add(
        ARoute("Settings", "Settings Screen", "/settings", const Settings()));

    // Set brightness 
    if (WidgetsBinding.instance.window.platformBrightness == Brightness.dark) {
      ref.read(themeProvider.notifier).changeBrightnessMode(Brightness.dark);
    }

    // Monitor network connectivity 
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult connectivityResult) {
      ref
          .read(networkConnectionStateProvider.notifier)
          .setNetworkStateByConnectivityResult(connectivityResult);
    });

    // Make logging available to key services
    GlobalEnvironmentValues.instance.setLogWriter(ref.read(logWriterProvider));

    //Add platform to GlobalEnv vars
    GlobalEnvironmentValues.instance.setPlatform(ref.read(platformProvider).toDlPlatformType());
  }

  // Change brightness if the client device changes it setting, allows for automatic changes for day and night.
  @override
  void didChangePlatformBrightness() {
    ref.read(themeProvider.notifier).changeBrightnessMode(
        WidgetsBinding.instance.window.platformBrightness);
    super.didChangePlatformBrightness();
  }

  // Send a notification when the application starts or wakes.
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    ref.read(appLifecycleStateProvider.notifier).setLifecycleState(state);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);

    super.dispose();

    subscription.cancel();
  }

  void removeSplashScreen(WidgetRef ref) {
    if (ref.read(applicationModeProvider) == ApplicationMode.running) {
      FlutterNativeSplash.remove();
    }
    ref.read(eventStoreProvider).bus.fire(RemoveSplashScreen());
  }

  /// Run the best application for the platform of the device using the application, to provide a better native experience.
  @override
  Widget build(BuildContext context) {
    removeSplashScreen(ref);
    return ref.watch(platformProvider) == UniversalPlatformType.IOS
        ? cupertinoApp(context)
        : materialApp(context);
  }

  /*
    #######  #####  
  # #     # #     # 
  # #     # #       
  # #     #  #####  
  # #     #       # 
  # #     # #     # 
  # #######  #####  
  */
  CupertinoApp cupertinoApp(BuildContext context) {
    var routeModels = ref.watch(routeRegistryProvider).routeList;
    return CupertinoApp.router(
      title: "Prologue",
      theme: ref.watch(themeProvider).cupertinoThemeData,
      routerConfig: RoutesToGoRouterAdapter(
        routeModels,
        "/login",
        navaigationObservers: [
          NavigationEventsObserver(ref.read(eventStoreProvider))
        ],
      ).goRouter,
    );
  }

  /*
     #                                         
    # #   #    # #####  #####  #  ####  #####  
  #    #  ##   # #    # #    # # #    # #    # 
  #     # # #  # #    # #    # # #    # #    # 
  ####### #  # # #    # #####  # #    # #    # 
  #     # #   ## #    # #   #  # #    # #    # 
  #     # #    # #####  #    # #  ####  #####  
  */
  MaterialApp materialApp(BuildContext context) {
    var routeModels = ref.watch(routeRegistryProvider).routeList;
    return MaterialApp.router(
      title: "Prologue",
      theme: ref.watch(themeProvider).materialThemeData,
      routerConfig: RoutesToGoRouterAdapter(
        routeModels,
        "/",
        navaigationObservers: [
          NavigationEventsObserver(ref.read(eventStoreProvider))
        ],
      ).goRouter,
    );
  }
}
