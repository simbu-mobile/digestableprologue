import 'package:digestable_prologue/security/service/secure_route_service.dart';
import 'package:flutter_test/flutter_test.dart';

//
// #     #                   ####### 
// #     # #    # # #####       #    ######  ####  ##### 
// #     # ##   # #   #         #    #      #        #   
// #     # # #  # #   #         #    #####   ####    #   
// #     # #  # # #   #         #    #           #   #   
// #     # #   ## #   #         #    #      #    #   #   
//  #####  #    # #   #         #    ######  ####    #   
//

// #######
//    #    ######  ####  #####
//    #    #      #        #
//    #    #####   ####    #
//    #    #           #   #
//    #    #      #    #   #
//    #    ######  ####    #

void main() {
    group('Secure Route Service: ', () {
        canIdentifyASecuredRoot();
        identifiesLoginAsUnrestricted();
    });
}

Future<void> canIdentifyASecuredRoot() async {
   return test('Can identify a secured route by route name', () {
      var isRestricted = SecureRouteService().isRestricted("Recipes");
      expect(isRestricted, true);
   });
}

Future<void> identifiesLoginAsUnrestricted() async {
   return test('Knows the login screen is unrestricted', () {
      var isRestricted = SecureRouteService().isRestricted("Login");
      expect(isRestricted, false);
   });
}