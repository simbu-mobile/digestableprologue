# Digestable Prologue

Splash, login, native layout and other boring stuff we need to get started with an app.

## Features

### Version 1.0.0

* Adaptive themes
* Splash screen

## Getting started

See `/example` folder.

## Additional information

Pull requests welcome.
