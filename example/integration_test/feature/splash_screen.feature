Feature: Splash Screen
	Scenario: Displayed at startup
		Given: The user starts the application
		When: The application is loading
		Then: The splash screen is displayed
    Scenario: Hidden after loading
		Given: The user starts the application
		When: The application has finished loading
		Then: The splash screen is hidden