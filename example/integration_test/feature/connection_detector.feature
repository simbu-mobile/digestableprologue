Feature: Connection Detector
	Scenario: iPhone Offline
		Given: An iPhone in Airplane mode
		When: The application is started
		Then: Network connection is Offline
    Scenario: Android Phone Offline
		Given: A Samsung phone in Airplane mode
		When: The application is started
		Then: Network connection is Offline
	Scenario: Browser Offline
		Given: A browser without a network connection
		When: The application is started
		Then: Network connection is Offline
	Scenario: iPhone Online - Wifi
		Given: An iPhone connected to wifi
		When: The application is started
		Then: Network connection is Online
    Scenario: Android Phone Online - Wifi
		Given: The Samsung phone is connected to wifi
		When: The application is started
		Then: Network connection is Online
	Scenario: iPhone Online - Celluler
		Given: An iPhone connected to a celluler network
		When: The application is started
		Then: Network connection is Online
    Scenario: Android Phone Online - Celluler
		Given: A Samsung phone is connected to a celluler network
		When: The application is started
		Then: Network connection is Online
	Scenario: Browser Online - Wifi
		Given: A browser is connected to wifi
		When: The application is started
		Then: Network connection is Online
	Scenario: Browser Online - Lan
		Given: A browser is connected to the Lan
		When: The application is started
		Then: Network connection is Online