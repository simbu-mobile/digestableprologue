Feature: Adaptive Theme
    @IPhone
    Scenario: Light Theme 
		Given: An Apple iPhone
        And: The brightness setting is set to Light
		When: The application is started
		Then: The Light Theme is used
    @IPhone
	Scenario: Whitelabelled Light Theme
		Given: An Apple iPhone
        And: The application is whitelabelled
        And: The brightness setting is set to Light
		When: The application is started
		Then: The Whitelabelled Light Theme is used
    @IPhone
    Scenario: Dark Theme 
		Given: An Apple iPhone
        And: The brightness setting is set to Dark
		When: The application is started
		Then: The Dark Theme is used
    @IPhone
	Scenario: Whitelabelled Dark Theme
		Given: An Apple iPhone
        And: The application is whitelabelled
        And: The brightness setting is set to Dark
		When: The application is started
		Then: The Whitelabelled Dark Theme is used
    @IPhone
	Scenario: Respond to Brightness Setting Change
		Given: An Apple iPhone
		When: The the phones brightness settting changes
		Then: The theme is changes
    @Android
    Scenario: Light Theme 
		Given: An Android phone
        And: The brightness setting is set to Light
		When: The application is started
		Then: The Light Theme is used
    @Android
	Scenario: Whitelabelled Light Theme
		Given: An Android phone
        And: The application is whitelabelled
        And: The brightness setting is set to Light
		When: The application is started
		Then: The Whitelabelled Light Theme is used
    @Android
    Scenario: Dark Theme 
		Given: An Android phone
        And: The brightness setting is set to Dark
		When: The application is started
		Then: The Dark Theme is used
    @Android
	Scenario: Whitelabelled Dark Theme
		Given: An Android phone
        And: The application is whitelabelled
        And: The brightness setting is set to Dark
		When: The application is started
		Then: The Whitelabelled Dark Theme is used
    @Android
	Scenario: Respond to Brightness Setting Change
		Given: An Android phone
		When: The the phones brightness settting changes
		Then: The theme is changes