Feature: Stubbed Authentication Service
    Scenario: Authentication stubbed out for development
        Given: The development environment
        And: Authentication is required
		    When: The application is started
		    Then: The user is automatically authenticated
    Scenario: Authentication stubbed out for automated tests
        Given: The automated test environment 
        And: Authentication is required
		    When: The application is started
		    Then: The user is automatically authenticated
    Scenario: Logout available but no action taken
        Given: An environment that uses stubbed authentication
        When: We navigate to '/settings'
        And: I tap the label that contains the text 'Sign Out'
      	Then: A 'SignedOut' event fires 
        And: The user remains authenticated