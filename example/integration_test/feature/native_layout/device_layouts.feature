Feature: Layout Selection
    @IPhone
    Scenario: Tabbed Layout for iPhones 
		Given: An Apple iPhone
		When: The application is started
		Then: A Cupertino tabbed layout is displayed
    @Android
	Scenario: Side draw Layout for Android devices 
		Given: A device running Android
		When: The application is started
		Then: A Material side draw layout is displayed
    @Android
    Scenario: Native experience for Android devices 
		Given: A device running Android
		When: The application is started
		Then: The Material Application is used
    @Ios
	Scenario: Native experience for iPhones and iPads 
		Given: A device running iOS
		When: The application is started
		Then: The Cupertino Application is used