import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:gherkin/gherkin.dart';

// The application under test.
import 'app_runner.dart' as app;

// Feature steps
import 'package:simbucore/integration_test/gherkin/builder/config_builder.dart';
import 'steps/app/application_steps.dart';
import 'steps/app/environment_steps.dart';
import 'steps/app/event_steps.dart';
import 'steps/app/route_steps.dart';
import 'steps/app/setting_steps.dart';
import 'steps/security/authentication_steps.dart';

part 'gherkin_feature_test.g.dart';

const String featureUnderTest = "feature/security/authentication_service.feature";

Iterable<StepDefinitionGeneric> stepDefinitions = [
  ...applicationSteps(),
  ...authenicationSteps(),
  ...environmentSteps(),
  ...settingSteps(),
  ...routeSteps(),
  ...eventSteps(),
];

@GherkinTestSuite(featurePaths: ['integration_test/$featureUnderTest',])
void main() {
  final testConfig = gherkinTestConfiguration([RegExp(featureUnderTest),], stepDefinitions);

  executeTestSuite(configuration: testConfig, appMainFunction: (World world) => app.runner());
}