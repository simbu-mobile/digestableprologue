//https://giters.com/jonsamwell/flutter_gherkin/issues/193
//import 'package:flutter_gherkin/flutter_gherkin_integration_test.dart'; // notice new import name
// ignore_for_file: avoid_print
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:gherkin/gherkin.dart';

// The application under test.
import 'app_runner.dart' as app;

// Feature steps
import 'package:simbucore/integration_test/gherkin/builder/config_builder.dart';
import 'steps/app/api_steps.dart';
import 'steps/app/application_steps.dart';
import 'steps/app/environment_steps.dart';
import 'steps/app/event_steps.dart';
import 'steps/app/route_steps.dart';
import 'steps/app/setting_steps.dart';
import 'steps/security/authentication_steps.dart';

part 'gherkin_live_env_test.g.dart';

Iterable<StepDefinitionGeneric> stepDefinitions = [
  ...applicationSteps(),
  ...authenicationSteps(),
  ...environmentSteps(),
  ...settingSteps(),
  ...routeSteps(),
  ...eventSteps(),
  ...apiSteps(),
];

@GherkinTestSuite(featurePaths: ['integration_test/feature_live_env/**.feature'])
void main() {
  final testConfig = gherkinTestConfiguration([RegExp('feature_live_env/*.*.feature'),], stepDefinitions);

  executeTestSuite(configuration: testConfig, appMainFunction: (World world) => app.runner(liveEnv: true));
}