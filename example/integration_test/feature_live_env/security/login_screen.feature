Feature: Login Screen
    Scenario: Shown on start-up when not authenticated 
		Given: The live environment
		And: Authentication is required
        And: Not authenticated
		When: The application is started
		Then: Navigates to '/login'
    Scenario: Shown on wake when not authenticated
		Given: The live environment
		And: Authentication is required
        And: Not authenticated
		When: The application awakes
		Then: Navigates to '/login'
    Scenario: Shown when making an API request when not authenticated
		Given: The live environment
		And: Authentication is required
        And: Not authenticated
		When: Making an API request
		Then: Navigates to '/login'
    Scenario: Not shown on start-up when authenticated
		Given: The live environment
		And: Authentication is required
        And: Is Authenticated
		When: We display the home screen
		Then: Does not navigate to the '/login'
    Scenario: Not shown when making an API request and authenticated 
		Given: The live environment
		And: Authentication is required
        And: Is Authenticated
		When: Making an API request
		Then: Does not navigate to the '/login'