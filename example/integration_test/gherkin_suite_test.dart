//https://giters.com/jonsamwell/flutter_gherkin/issues/193
//import 'package:flutter_gherkin/flutter_gherkin_integration_test.dart'; // notice new import name
// ignore_for_file: avoid_print
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:gherkin/gherkin.dart';

// The application under test.
import 'app_runner.dart' as app;

// Feature steps
import 'package:simbucore/integration_test/gherkin/builder/config_builder.dart';
import 'package:simbucore/integration_test/gherkin/builder/feature_builder.dart';
import 'steps/step_builder.dart';

part 'gherkin_suite_test.g.dart';

@GherkinTestSuite(featurePaths: featurePaths)
void main() {
  final testConfig = gherkinTestConfiguration(features, stepDefinitions);

  executeTestSuite(configuration: testConfig, appMainFunction: (World world) => app.runner());
}