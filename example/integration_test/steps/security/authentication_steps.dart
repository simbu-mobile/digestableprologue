import 'package:digestable_prologue/prologue_app.dart';
import 'package:digestable_prologue/security/model/authentication_mode.dart';
import 'package:digestable_prologue/security/provider/authentication_provider.dart';
import 'package:simbucore/integration_test/gherkin/extension/riverpod_provider.dart';
import 'package:gherkin/gherkin.dart';
import 'package:simbucore/integration_test/gherkin/builder/extended_world_builder.dart';

Iterable<StepDefinitionGeneric> authenicationSteps() => [
      givenAlwaysOnAuth(),
      andAlwaysOnAuth(),
      andAuthenticated(),
      andNotAuthenticated(),
      thenAutoAuthenticated(),
      thenRemainsAuthenticated(),
    ];

StepDefinitionGeneric givenAlwaysOnAuth() {
  return given<ExtendedWorld>(
    'Authentication is required',
    (context) async {
      context.world.appDriver
          .providerRef<PrologueApp>()
          .read(authenticationProvider.notifier)
          .setAuthenicationMode(AuthenticationMode.alwaysOn);
    },
  );
}

StepDefinitionGeneric andAlwaysOnAuth() {
  return and<ExtendedWorld>(
    'Authentication is required',
    (context) async {
      context.world.appDriver
          .providerRef<PrologueApp>()
          .read(authenticationProvider.notifier)
          .setAuthenicationMode(AuthenticationMode.alwaysOn);
    },
  );
}

StepDefinitionGeneric andAuthenticated() {
  return given<ExtendedWorld>(
    'Is Authenticated',
    (context) async {
      context.world.appDriver
          .providerRef<PrologueApp>()
          .read(authenticationProvider.notifier)
          .storeAccessToken("token");
    },
  );
}

StepDefinitionGeneric andNotAuthenticated() {
  return given<ExtendedWorld>(
    'Not authenticated',
    (context) async {
      context.world.appDriver
          .providerRef<PrologueApp>()
          .read(authenticationProvider.notifier)
          .storeAccessToken("token");
    },
  );
}

StepDefinitionGeneric thenAutoAuthenticated() {
  return then<ExtendedWorld>(
    'The user is automatically authenticated',
    (context) async {
      var authenticated = context.world.appDriver
          .providerRef<PrologueApp>()
          .read(authenticationProvider).isAuthenticated;
      assert(authenticated, true);
    },
  );
}

StepDefinitionGeneric thenRemainsAuthenticated() {
  return then<ExtendedWorld>(
    'The user remains authenticated',
    (context) async {
      var authenticated = context.world.appDriver
          .providerRef<PrologueApp>()
          .read(authenticationProvider).isAuthenticated;
      assert(authenticated, true);
    },
  );
}