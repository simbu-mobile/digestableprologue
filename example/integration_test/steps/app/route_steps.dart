import 'package:digestable_prologue/native_layout/widget/layout_selector.dart';
import 'package:digestable_prologue/security/widget/login.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';
import 'package:digestable_prologue/prologue_app.dart';
import 'package:simbucore/app/event_store/provider/event_provider.dart';
import 'package:simbucore/app/routes/event/navigated.dart';
import 'package:simbucore/integration_test/gherkin/extension/riverpod_provider.dart';
import 'package:simbucore/integration_test/gherkin/extension/router_extension.dart';

Iterable<StepDefinitionGeneric> routeSteps() => [
      whenDisplayRestrictedScreen(),
      whenDisplayUnrestrictedScreen(),
      whenWeDisplayHomeScreen(),
      weNavigateTo(),
      thenNavigates(),
      thenDoesNotNavigate(),
    ];

StepDefinitionGeneric whenDisplayRestrictedScreen() {
  return when<FlutterWorld>(
    'Displaying a restricted screen',
    (context) async {
      var eventStore = context.world.appDriver
          .providerRef<PrologueApp>()
          .read(eventStoreProvider);

      eventStore.bus.fire(Navigated("Recipes"));
    },
  );
}

StepDefinitionGeneric whenDisplayUnrestrictedScreen() {
  return when<FlutterWorld>(
    'Displaying an unrestricted screen',
    (context) async {
      var eventStore = context.world.appDriver
          .providerRef<PrologueApp>()
          .read(eventStoreProvider);

      eventStore.bus.fire(Navigated("Login"));
    },
  );
}

StepDefinitionGeneric whenWeDisplayHomeScreen() {
  return when<FlutterWorld>(
    'We display the home screen',
    (context) async {
      context.world.appDriver.pushRoute<Login>("/");
      await context.world.appDriver.waitForAppToSettle();
    },
  );
}

StepDefinitionGeneric weNavigateTo() {
  return when1<String, FlutterWorld>(
    'We navigate to {string}',
    (routeName, context) async {
      context.world.appDriver.pushRoute<LayoutSelector>(routeName);
      await context.world.appDriver.waitForAppToSettle();
    },
  );
}

StepDefinitionGeneric thenNavigates() {
  return then1<String, FlutterWorld>(
    'Navigates to {string}',
    (routeName, context) async {
      var eventStore = context.world.appDriver
          .providerRef<PrologueApp>()
          .read(eventStoreProvider);

      context.expect(
        eventStore.lastNavigationEvent?.routeName,
        routeName,
      );
      // Integrate routing event here.
    },
  );
}

StepDefinitionGeneric thenDoesNotNavigate() {
  return then1<String, FlutterWorld>(
    'Does not navigate to the {string}',
    (routeName, context) async {
      var eventStore = context.world.appDriver
          .providerRef<PrologueApp>()
          .read(eventStoreProvider);

      context.expect(
        eventStore.lastNavigationEvent?.routeName != routeName,
        true,
      );
      // Integrate routing event here.
    },
  );
}
