import 'package:digestable_prologue/prologue_app.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';
import 'package:simbucore/app/event_store/provider/event_provider.dart';
import 'package:simbucore/integration_test/gherkin/extension/riverpod_provider.dart';

Iterable<StepDefinitionGeneric> eventSteps() => [
  eventFired()
];

StepDefinitionGeneric eventFired() { 
   return then1<String, FlutterWorld>(
      'A {string} event fires',
      (eventName, context) async {
        
        var eventStore = context.world.appDriver.providerRef<PrologueApp>().read(eventStoreProvider);

         var signedOutEventCount = eventStore.eventCounter[eventName] ?? 0;

         assert(signedOutEventCount > 0, true);
      },
   );
}