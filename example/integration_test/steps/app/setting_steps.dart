import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

Iterable<StepDefinitionGeneric> settingSteps() => [
  pressLogoutButton()
];

StepDefinitionGeneric pressLogoutButton() { 
   return when<FlutterWorld>(
      'Press the logout button',
      (context) async {
        
        //context.world.appDriver.providerRef<PrologueApp>().read(themeProvider.notifier).changeBrightnessMode(Brightness.dark);
      },
   );
}