import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simbucore/integration_test/gherkin/builder/extended_world_builder.dart';

Iterable<StepDefinitionGeneric> platformSteps() => [
      givenADeviceRunningiOs(),
      givenADeviceRunningAndroid(),
      thenACupertinoAppIsUsed(),
      thenAMaterialAppIsUsed(),
    ];

StepDefinitionGeneric givenADeviceRunningiOs() {
  return given<ExtendedWorld>(
    'A device running iOS',
    (context) async {
      
      context.expect(context.world.isIos, true);
    },
  );
}

StepDefinitionGeneric givenADeviceRunningAndroid() {
  return given<ExtendedWorld>(
    'A device running Android',
    (context) async {
      context.expect(context.world.isAndroid, true);
    },
  );
}

StepDefinitionGeneric thenACupertinoAppIsUsed() {
  return then<ExtendedWorld>(
    'The Cupertino Application is used',
    (context) async {
      final cupertinoAppFound =
          await context.world.appDriver.isPresent(find.byType(CupertinoApp));
      context.expect(
        cupertinoAppFound,
        true,
      );
    },
  );
}

StepDefinitionGeneric thenAMaterialAppIsUsed() {
  return then<ExtendedWorld>(
    'The Material Application is used',
    (context) async {
      final materialAppFound =
          await context.world.appDriver.isPresent(find.byType(MaterialApp));
      context.expect(
        materialAppFound,
        true,
      );
    },
  );
}