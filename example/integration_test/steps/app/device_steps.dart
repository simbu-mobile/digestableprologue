import 'package:gherkin/gherkin.dart';
import 'package:simbucore/app/core/service/device_adapter.dart';
import 'package:simbucore/integration_test/gherkin/builder/extended_world_builder.dart';

Iterable<StepDefinitionGeneric> deviceSteps() => [
      givenAnIphone(),
      givenAnIpad(),
    ];

StepDefinitionGeneric givenAnIphone() {
  return given<ExtendedWorld>(
    'An Apple iPhone',
    (context) async {
      var device = await DeviceAdapter().getDevice();
      context.expect(device.isIPhone, true);
    },
  );
}

StepDefinitionGeneric givenAnIpad() {
  return given<ExtendedWorld>(
    'An iPad',
    (context) async {
      // Ignoring as it doesn't make sense to make this check as we only run 
      // these feature tests on an iPad, see from the logic below. 

      // if (context.ignoreOtherPlatforms(UniversalPlatformType.IOS)) {
      //   return;
      // }

      // var provider = context.provider<StartUp>();

      // if(!provider.isIPad){
      //   return;
      // }
      
      // context.expect(provider.isIPad, true);
    },
  );
}
