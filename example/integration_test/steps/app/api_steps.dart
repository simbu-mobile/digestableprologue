import 'package:digestable_prologue/prologue_app.dart';
import 'package:gherkin/gherkin.dart';
import 'package:simbucore/app/data_access/event/api_request_made.dart';
import 'package:simbucore/app/event_store/provider/event_provider.dart';

import 'package:simbucore/integration_test/gherkin/builder/extended_world_builder.dart';
import 'package:simbucore/integration_test/gherkin/extension/riverpod_provider.dart';

Iterable<StepDefinitionGeneric> apiSteps() => [
      whenMakingAPIRequest(),
    ];

StepDefinitionGeneric whenMakingAPIRequest() {
  return when<ExtendedWorld>(
    'Making an API request',
    (context) async {
      var eventStore = context.world.appDriver
          .providerRef<PrologueApp>()
          .read(eventStoreProvider);

      eventStore.bus.fire(ApiRequestMade("https://api.simbu.co.uk/digests/recipes"));
      await context.world.appDriver.waitForAppToSettle();
    },
  );
} 