import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

Iterable<StepDefinitionGeneric> environmentSteps() => [
      givenTheDevelopmentEnvironment(),
      givenTheAutomatedTestEnvironment(),
      givenAnEnvUsingStubbedAuth(),
      givenTheLiveEnvironment(),
];

StepDefinitionGeneric givenTheDevelopmentEnvironment() {
  return given<FlutterWorld>(
    'The development environment',
    (context) async {
      // Here for readability
      // Controlled by test suite/app_runner as its too late to change in code
    },
  );
}

StepDefinitionGeneric givenTheLiveEnvironment() {
  return given<FlutterWorld>(
    'The live environment',
    (context) async {
      // Here for readability
      // Controlled by test suite/app_runner as its too late to change in code
    },
  );
}

StepDefinitionGeneric givenTheAutomatedTestEnvironment() {
  return given<FlutterWorld>(
    'The automated test environment',
    (context) async {
      // Here for readability
      // Controlled by test suite/app_runner as its too late to change in code
    },
  );
}

StepDefinitionGeneric givenAnEnvUsingStubbedAuth() {
  return given<FlutterWorld>(
    'An environment that uses stubbed authentication',
    (context) async {
      // Here for readability
      // Controlled by test suite/app_runner as its too late to change in code
    },
  );
}