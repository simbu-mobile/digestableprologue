import 'package:digestable_prologue/prologue_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';
import 'package:simbucore/integration_test/gherkin/extension/riverpod_provider.dart';
import 'package:simbucore/theme/provider/theme_provider.dart';

Iterable<StepDefinitionGeneric> brightnessSteps() => [
  whenBrightnessSettingChanges()
];

StepDefinitionGeneric whenBrightnessSettingChanges() { 
   return when<FlutterWorld>(
      'The the phones brightness settting changes',
      (context) async {
        context.world.appDriver.providerRef<PrologueApp>().read(themeProvider.notifier).changeBrightnessMode(Brightness.dark);
      },
   );
}