import 'package:digestable_prologue/prologue_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';
import 'package:simbucore/app/core/provider/app_lifecycle_state_provider.dart';

import 'package:simbucore/integration_test/gherkin/builder/extended_world_builder.dart';
import 'package:simbucore/integration_test/gherkin/extension/riverpod_provider.dart';

Iterable<StepDefinitionGeneric> applicationSteps() => [
      givenTheUserStartsTheApplication(),
      whenTheApplicationAwakes(),
      whenTheApplicationIsStarted(),
      whenTheApplicationIsLoading(),
      whenTheApplicationHasFinishedLoading(),
    ];

StepDefinitionGeneric givenTheUserStartsTheApplication() {
  return given<ExtendedWorld>(
    'The user starts the application',
    (context) async {
      // Do Nothing it is restarted by default for each scenario.
    },
  );
}

// 'It is not the first time the app is used'
// 'It is the first time the application is run after installation'
// 'And: It has not been updated prior to running'
// Given: It is first time the app is used after an update`
// `Given: The user has not used the application before`
// `And: The user is authenticated`

StepDefinitionGeneric whenTheApplicationIsStarted() {
  return when<FlutterWorld>(
    'The application is started',
    (context) async {
      // Do Nothing it is restarted by default for each scenario.
    },
  );
}

StepDefinitionGeneric whenTheApplicationAwakes() {
  return when<FlutterWorld>(
    'The application awakes',
    (context) async {
      context.world.appDriver
          .providerRef<PrologueApp>()
          .read(appLifecycleStateProvider.notifier)
          .setLifecycleState(AppLifecycleState.resumed);
    },
  );
}

StepDefinitionGeneric whenTheApplicationIsLoading() {
  return when<FlutterWorld>(
    'The application is loading',
    (context) async {
      // Do Nothing it is restarted by default for each scenario.
    },
  );
}

StepDefinitionGeneric whenTheApplicationHasFinishedLoading() {
  return when<FlutterWorld>(
    'The application has finished loading',
    (context) async {
      // Do Nothing it is restarted by default for each scenario.
    },
  );
}


