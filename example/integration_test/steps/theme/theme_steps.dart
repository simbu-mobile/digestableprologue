import 'dart:ui';

import 'package:digestable_prologue/prologue_app.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';
import 'package:simbucore/integration_test/gherkin/extension/riverpod_provider.dart';
import 'package:simbucore/theme/provider/theme_provider.dart';
import 'package:simbucore/theme/themer.dart';

Iterable<StepDefinitionGeneric> themeSteps() => [
  andAppIsWhitelabelled(),
  thenLightThemeUsed(),
  thenDarkThemeUsed(),
  thenWhitelabelledLightThemeUsed(),
  thenWhitelabelledDarkThemeUsed(),
];

StepDefinitionGeneric andAppIsWhitelabelled() { 
   return and<FlutterWorld>(
      'The application is whitelabelled',
      (context) async {
         context.world.appDriver.providerRef<PrologueApp>().read(themeProvider.notifier).changeWhitelabel(WhiteLabel.ee);
      },
   );
}

StepDefinitionGeneric thenLightThemeUsed() { 
   return then<FlutterWorld>(
      'The Light Theme is used',
      (context) async {
        var theme = context.world.appDriver.providerRef<PrologueApp>().read(themeProvider);
        var isLightTheme = theme.brightness == Brightness.light && theme.whitelabel == WhiteLabel.none;
        context.expect(isLightTheme, true);
      },
   );
}

StepDefinitionGeneric thenDarkThemeUsed() { 
   return then<FlutterWorld>(
      'The Dark Theme is used',
      (context) async {
        var theme = context.world.appDriver.providerRef<PrologueApp>().read(themeProvider);
        var isDarkTheme = theme.brightness == Brightness.dark && theme.whitelabel == WhiteLabel.none;
        context.expect(isDarkTheme, true);
      },
   );
}

StepDefinitionGeneric thenWhitelabelledLightThemeUsed() { 
   return then<FlutterWorld>(
      'The Whitelabelled Light Theme is used',
      (context) async {
        var theme = context.world.appDriver.providerRef<PrologueApp>().read(themeProvider);
        var isWhitelabelledLightTheme = theme.brightness == Brightness.light && theme.whitelabel == WhiteLabel.ee;
        context.expect(isWhitelabelledLightTheme, true);
      },
   );
}

StepDefinitionGeneric thenWhitelabelledDarkThemeUsed() { 
   return then<FlutterWorld>(
      'The Whitelabelled Dark Theme is used',
      (context) async {
         var theme = context.world.appDriver.providerRef<PrologueApp>().read(themeProvider);
        var isWhitelabelledDarkTheme = theme.brightness == Brightness.dark && theme.whitelabel == WhiteLabel.ee;
        context.expect(isWhitelabelledDarkTheme, true);
      },
   );
}
