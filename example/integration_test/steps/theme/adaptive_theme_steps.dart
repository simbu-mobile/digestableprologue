import 'dart:ui';

import 'package:digestable_prologue/prologue_app.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';
import 'package:simbucore/integration_test/gherkin/extension/riverpod_provider.dart';
import 'package:simbucore/theme/provider/theme_provider.dart';

Iterable<StepDefinitionGeneric> adaptiveThemeSteps() => [
  andBrightnessSettingSetToLight(),
  andBrightnessSettingSetToDark(),
  thenThemeChanges(),
];

StepDefinitionGeneric andBrightnessSettingSetToLight() { 
   return and<FlutterWorld>(
      'The brightness setting is set to Light',
      (context) async {
        context.world.appDriver.providerRef<PrologueApp>().read(themeProvider.notifier).changeBrightnessMode(Brightness.light);
      },
   );
}

StepDefinitionGeneric andBrightnessSettingSetToDark() { 
   return and<FlutterWorld>(
      'And: The brightness setting is set to Dark',
      (context) async {
        context.world.appDriver.providerRef<PrologueApp>().read(themeProvider.notifier).changeBrightnessMode(Brightness.dark);
      },
   );
}

StepDefinitionGeneric thenThemeChanges() { 
   return then<FlutterWorld>(
      'The theme is changes',
      
      (context) async {
        var theme = context.world.appDriver.providerRef<PrologueApp>().read(themeProvider);
        context.expect(theme.brightness, Brightness.dark);
      },
   );
}

