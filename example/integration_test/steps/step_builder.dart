import 'package:gherkin/gherkin.dart';

import 'app/api_steps.dart';
import 'app/environment_steps.dart';
import 'app/route_steps.dart';
import 'app/brightness_steps.dart';
import 'app/application_steps.dart';
import 'app/platform_steps.dart';
import 'app/device_steps.dart';
import 'app/setting_steps.dart';
import 'app/event_steps.dart';
import 'connection_detector/connection_steps.dart';
import 'native_layout/layout_steps.dart';
import 'security/authentication_steps.dart';
import 'splash_screen/splash_screen_steps.dart';
import 'theme/adaptive_theme_steps.dart';
import 'theme/theme_steps.dart';

Iterable<StepDefinitionGeneric> stepDefinitions = [
  ...applicationSteps(),
  ...platformSteps(),
  ...deviceSteps(),
  ...layoutSteps(),
  ...splashSteps(),
  ...brightnessSteps(),
  ...themeSteps(),
  ...adaptiveThemeSteps(),
  ...connectionSteps(),
  ...routeSteps(),
  ...settingSteps(),
  ...eventSteps(),
  ...authenicationSteps(),
  ...apiSteps(),
  ...environmentSteps(),
];
