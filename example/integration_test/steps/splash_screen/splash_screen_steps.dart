import 'package:digestable_prologue/prologue_app.dart';
import 'package:simbucore/app/event_store/provider/event_provider.dart';
import 'package:simbucore/integration_test/gherkin/extension/riverpod_provider.dart';
import 'package:gherkin/gherkin.dart';
import 'package:simbucore/integration_test/gherkin/builder/extended_world_builder.dart';

Iterable<StepDefinitionGeneric> splashSteps() => [
      thenTheSplashScreenIsDisplayed(),
      thenTheSplashScreenIsHidden(),
    ];

StepDefinitionGeneric thenTheSplashScreenIsDisplayed() {
  return then<ExtendedWorld>(
    'The splash screen is displayed',
    (context) async {
      var eventName = "PreserveSplashScreen";
      var eventCount = context.world.appDriver.providerRef<PrologueApp>().read(eventStoreProvider).eventCounter[eventName];

      context.expect(
        eventCount != null && eventCount > 0,
        true,
      );
    },
  );
}

StepDefinitionGeneric thenTheSplashScreenIsHidden() {
  return then<ExtendedWorld>(
    'The splash screen is hidden',
    (context) async {
      var eventName = "RemoveSplashScreen";
      var eventCount = context.world.appDriver.providerRef<PrologueApp>().read(eventStoreProvider).eventCounter[eventName];
     
      context.expect(
        eventCount != null && eventCount > 0,
        true,
      );
    },
  );
}

