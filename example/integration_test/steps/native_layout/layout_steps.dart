import 'package:layout_lego/dl_bottom_tab_layout.dart';
import 'package:digestable_prologue/native_layout/sidedraw/widget/android_sidedraw_layout.dart';
import 'package:gherkin/gherkin.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simbucore/integration_test/gherkin/builder/extended_world_builder.dart';

Iterable<StepDefinitionGeneric> layoutSteps() => [
      thenACupertinoTabbedLayoutIsDisplayed(),
      thenAMaterialSideDrawLayoutIsDisplayed(),
    ];

StepDefinitionGeneric thenACupertinoTabbedLayoutIsDisplayed() {
  return then<ExtendedWorld>(
    'A Cupertino tabbed layout is displayed',
    (context) async {
      final cupertinoLayoutFound =
          await context.world.appDriver.isPresent(find.byType(IosTabbedLayout));
      context.expect(
        cupertinoLayoutFound,
        true,
      );
    },
  );
}

StepDefinitionGeneric thenAMaterialSideDrawLayoutIsDisplayed() {
  return then<ExtendedWorld>(
    'A Material side draw layout is displayed',
    (context) async {
      final aMaterialLayoutFound =
          await context.world.appDriver.isPresent(find.byType(SidedrawLayout));
      context.expect(
        aMaterialLayoutFound,
        true,
      );
    },
  );
}