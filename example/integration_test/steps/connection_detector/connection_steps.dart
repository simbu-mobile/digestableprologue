import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';
import 'package:simbucore/integration_test/gherkin/extension/riverpod_provider.dart';
import 'package:digestable_prologue/connection_detector/model/network_connection_state.dart';
import 'package:digestable_prologue/connection_detector/provider/network_connection_state_provider.dart';
import 'package:digestable_prologue/prologue_app.dart';

Iterable<StepDefinitionGeneric> connectionSteps() => [
      giveniphoneInAirplaneMode(),
      giveniPhoneOnlineWifi(),
      giveniPhoneOnlineCell(),
      
      givenAnAndroidPhoneInAirplaneMode(),
      givenAndroidPhoneOnlineWifi(),
      givenAndroidPhoneOnlineCell(),

      givenBrowserOffline(),
      givenBrowserOnWifi(),
      givenBrowserOnLan(),

      thenNetworkConnetionOffline(),
      thenNetworkConnetionOnline(),
    ];

StepDefinitionGeneric giveniphoneInAirplaneMode() {
  return given<FlutterWorld>(
    'An iPhone in Airplane mode',
    (context) async {
      setAirplaneMode(context);
    },
  );
}

StepDefinitionGeneric givenAnAndroidPhoneInAirplaneMode() {
  return given<FlutterWorld>(
    'A Samsung phone in Airplane mode',
    (context) async {
      setAirplaneMode(context);
    },
  );
}

StepDefinitionGeneric givenBrowserOffline() {
  return given<FlutterWorld>(
    'A browser without a network connection',
    (context) async {
      noNetwork(context);
    },
  );
}

StepDefinitionGeneric giveniPhoneOnlineWifi() {
  return given<FlutterWorld>(
    'An iPhone connected to wifi',
    (context) async {
      connectedToWifi(context);
    },
  );
}

StepDefinitionGeneric givenAndroidPhoneOnlineWifi() {
  return given<FlutterWorld>(
    'The Samsung phone is connected to wifi',
    (context) async {
      connectedToWifi(context);
    },
  );
}

StepDefinitionGeneric giveniPhoneOnlineCell() {
  return given<FlutterWorld>(
    'An iPhone connected to a celluler network',
    (context) async {
      connectedToCell(context);
    },
  );
}

StepDefinitionGeneric givenAndroidPhoneOnlineCell() {
  return given<FlutterWorld>(
    'A Samsung phone is connected to a celluler network',
    (context) async {
      connectedToCell(context);
    },
  );
}

StepDefinitionGeneric givenBrowserOnWifi() {
  return given<FlutterWorld>(
    'A browser is connected to wifi',
    (context) async {
      connectedToWifi(context);
    },
  );
}

StepDefinitionGeneric givenBrowserOnLan() {
  return given<FlutterWorld>(
    'A browser is connected to the Lan',
    (context) async {
      connectedToLan(context);
    },
  );
}

StepDefinitionGeneric thenNetworkConnetionOffline() {
  return then<FlutterWorld>(
    'Network connection is Offline',
    (context) async {
      var networkConnectionState = context.world.appDriver
          .providerRef<PrologueApp>()
          .read(networkConnectionStateProvider);
      context.expect(networkConnectionState, NetworkConnectionState.offline);
    },
  );
}

StepDefinitionGeneric thenNetworkConnetionOnline() {
  return then<FlutterWorld>(
    'Network connection is Online',
    (context) async {
      var networkConnectionState = context.world.appDriver
          .providerRef<PrologueApp>()
          .read(networkConnectionStateProvider);
      context.expect(networkConnectionState, NetworkConnectionState.online);
    },
  );
}

void updateNetworkState(StepContext<FlutterWorld> context, ConnectivityResult connectivityResult) {
  context.world.appDriver
      .providerRef<PrologueApp>()
      .read(networkConnectionStateProvider.notifier)
      .setNetworkStateByConnectivityResult(connectivityResult);
}

void noNetwork(StepContext<FlutterWorld> context) {
  updateNetworkState(context, ConnectivityResult.none);
}

void setAirplaneMode(StepContext<FlutterWorld> context) {
  updateNetworkState(context, ConnectivityResult.none);
}

void connectedToWifi(StepContext<FlutterWorld> context) {
  updateNetworkState(context, ConnectivityResult.wifi);
}

void connectedToLan(StepContext<FlutterWorld> context) {
  updateNetworkState(context, ConnectivityResult.ethernet);
}

void connectedToCell(StepContext<FlutterWorld> context) {
  updateNetworkState(context, ConnectivityResult.mobile);
}
