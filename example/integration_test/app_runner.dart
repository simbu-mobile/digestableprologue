import 'package:digestable_prologue/native_layout/tabs/provider/tab_items_provider.dart';
import 'package:digestable_prologue/prologue_app.dart';
import 'package:example/app/data/tab_items.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:simbucore/app/core/service/global_environment_values.dart';
import 'package:simbucore/app/event_store/provider/event_provider.dart';
import 'package:simbucore/app/event_store/service/event_store.dart';
import 'package:simbucore/app/event_store/service/test_event_bus_service.dart';
import 'package:simbucore/app/core/model/application_mode.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:simbucore/app/core/provider/application_mode_provider.dart';

Future<void> runner({bool liveEnv = false}) async {
  WidgetsFlutterBinding.ensureInitialized();
  var configFile = liveEnv ? "app_config_live.json" : "app_config.json";
  GlobalEnvironmentValues.instance
      .loadValues(await rootBundle.loadString(configFile));
  main();
}

void main() {
  //IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  // Need to find a way to stop with error in IntegrationTestWidgetsFlutterBinding.tearDownAll await integrationTestChannel.invokeMethod<void>
  // MissingPluginException (MissingPluginException(No implementation found for method allTestsFinished on channel plugins.flutter.io/integration_test))

  runApp(
    ProviderScope(
      overrides: [
        applicationModeProvider
            .overrideWithProvider(StateProvider<ApplicationMode>(
          (ref) => ApplicationMode.testing,
        )),
        eventStoreProvider.overrideWithProvider(Provider<EventStore>(
          (ref) => EventStore(TestEventBusService()),
        )),
        tabItemsProvider.overrideWithProvider(
          StateNotifierProvider(
            (ref) => TabItemsNotifier(tabItems),
          ),
        ),
      ],
      child: const PrologueApp(),
    ),
  );
}
