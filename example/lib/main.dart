// Flutter imports:
import 'package:digestable_prologue/native_layout/tabs/provider/tab_items_provider.dart';
import 'package:example/app/data/tab_items.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:digestable_prologue/prologue_app.dart';
import 'package:flutter/services.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:simbucore/app/core/service/global_environment_values.dart';

/// Digestable Me application, A sharable collection of things of interest, music, food, travel, sport.
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  GlobalEnvironmentValues.instance
      .loadValues(await rootBundle.loadString("app_config_secret.json"));
  runApp(ProviderScope(
      overrides: [
        tabItemsProvider.overrideWithProvider(
          StateNotifierProvider((ref) => TabItemsNotifier(tabItems)),
        ),
      ],
      child: const PrologueApp(),
    ),
  );
}
