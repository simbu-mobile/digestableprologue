// Flutter imports:
import 'package:digestable_prologue/app/settings/widget/settings.dart';
import 'package:flutter/cupertino.dart';

// Package imports:
import 'package:layout_lego/models/dl_tab_item.dart';
import 'package:layout_lego/models/dl_navigation_bar.dart';


List<DlTabItem> get tabItems {
  return <DlTabItem>[
    DlTabItem(
      label: "Digests",
      icon: const Icon(CupertinoIcons.bars),
      content: Container(
        color: CupertinoColors.activeGreen,
      ),
      globalKey: GlobalKey<NavigatorState>(),
      navigationBar: const DlNavigationBar(
        heroTag: "RecipeTabHeroTag",
        title: "Recipes",
      ),
    ),
    DlTabItem(
      label: "Friends",
      icon: const Icon(CupertinoIcons.group),
      content: Container(
        color: CupertinoColors.activeOrange,
      ),
      globalKey: GlobalKey<NavigatorState>(),
      navigationBar: null
    ),
    DlTabItem(
      label: "History",
      icon: const Icon(CupertinoIcons.time),
      content: Container(
        color: CupertinoColors.destructiveRed,
      ),
      globalKey: GlobalKey<NavigatorState>(),
      navigationBar: null
    ),
    DlTabItem(
      label: "Settings",
      icon: const Icon(CupertinoIcons.settings),
      content: const Settings(),
      globalKey: GlobalKey<NavigatorState>(),
      navigationBar: null
    ),
  ];
}